#+TITLE: neo
[WIP]
* What is it?

[[./res/logo.png]]

neo (all lowercase) is a *visual programming language* and a *real time visualizer
and controller* for objects connected over a network.

** Programming interface

As a programming interface, neo allows the creation of complexed scenarios based
on behaviour of the objects (sensors) it is monitoring.

#+CAPTION: Figure 1
[[./res/buttonOrLightSensorDoorAllRed.png]]

In the above figure, the scenario involves a light sensor, a button and a door
connected through an OR gate. Whenever the button OR the light sensor change
in a certain way, the door is instructed to pose a predefined action.
 
** Visualizer

As a visualizer, neo makes it easy to track the status of sensors in real time.

#+CAPTION: Figure 2
[[./res/lightSensorOutputGreen.png]]

** Controller

As a controller, neo helps users intervening when things are not working the way
by allowing them to expressly instruct an object to execute a predefined action
without physical interaction.

#+CAPTION: Figure 3
[[./res/outputPopup.png]]

** All that and much more

These 3 functions are not everything. They can be used at the same or one at a
time. The interface is designed to be flexible and easy to modify.

* How is it made?

The program is made using QT. The UI is a combination of QML and JavsScript
while the application logic is written in C++.

Development is cuurently sponsored by [[https://www.missionsmorpheus.com/][Mission Morphéus]].

* Credits
- Logo and icon: [[https://nickmorris.netlify.com/][Nick Morris]]

* Usage and contributions

[[https://gitlab.com/hantz/Neo][neo on GitLab]]

*Coming soon.*
