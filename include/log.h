#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>

/*!
 * \class Log
 * \brief Represents a record to log.
 */
class Log : public QObject
{
  Q_OBJECT
public:
  /*!
   * \brief Enum used to specify the severity level of a logged event.
   */
  enum Level
  {
    Info,    /*!< Equivalent to a success message. */
    Debug,   /*!< Used for debugging. TODO: Disable on release. */
    Warning, /*!< Used to signal non-critical events. */
    Error    /*!< Used to signal critical events. */
  };
  Q_ENUM(Level)

  /*!
   * \brief Create a new record.
   * \param context The context of the event to log.
   * \param text The commennt to detail the event.
   * \param level The severity to assign the event.
   * \param parent QObject parent
   */
  explicit Log(QString context = "Unknown",
               QString text = "Empty",
               Log::Level level = Level::Info,
               QObject* parent = nullptr);

  Q_PROPERTY(QString text READ text NOTIFY textChanged)  /*!< \see Log::text */
  Q_PROPERTY(Level level READ level NOTIFY levelChanged) /*!< \see Log::level */

  /*!
   * \brief Get the text of the record.
   * \return A QString formed of the context, the text, and the severity level.
   */
  QString text();

  /*!
   * \brief Get the severity level
   * \return The severity level of the record.
   */
  Log::Level level();

signals:
  void textChanged(); /*!< Notify when the record's text have changed. */

  void levelChanged(); /*!< Notify when rhe record's severity have changed. */

public slots:

private:
  QString m_text;     /*!< Text of the record. */
  Log::Level m_level; /*!< Severity level of the record. */

  /*!
   * \brief Convert from the Log::Level enum to a QString
   * \param level The value to convert
   * \return The string representation of the enum value
   */
  QString levelToString(Log::Level level);
};

#endif // MESSAGE_H
