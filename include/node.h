#pragma once

#include <QHostAddress>
#include <QJsonArray>
#include <QJsonObject>
#include <QObject>
#include <QPoint>
#include <QQmlListProperty>
#include <QString>

#include "node.h"
#include "osc/reader/OscMessage.h"

/*!
 * \class Node
 * \brief The Node class
 *
 * This class is used as backend for all kind of nodes (Input, Output, Gates).
 */
class Node : public QObject
{
  Q_OBJECT

  Q_PROPERTY(Type type READ type WRITE setType NOTIFY
               typeChanged) /*!< \see Node::type */

  Q_PROPERTY(double value READ value WRITE setValue NOTIFY
               valueChanged) /*!< \see Node::value */
  Q_PROPERTY(bool output READ output WRITE setOutput NOTIFY
               outputChanged) /*!< \see Node::output */
  Q_PROPERTY(QString arguments READ arguments WRITE setArguments NOTIFY
               argumentsUpdated) /*!< \see Node::arguments */

  Q_PROPERTY(QString name READ name WRITE setName NOTIFY
               nameChanged) /*!< \see Node::read */
  Q_PROPERTY(QString address READ address WRITE setAddress NOTIFY
               addressChanged) /*!< \see Node::address */
  Q_PROPERTY(
    QString ip READ ip WRITE setIp NOTIFY ipChanged) /*!< \see Node::ip */

  Q_PROPERTY(bool bound READ bound WRITE setBound NOTIFY
               boundChanged) /*!< Node::bound */
  Q_PROPERTY(bool opened READ opened WRITE setOpened NOTIFY
               openedChanged) /*!< Node::opened */
  Q_PROPERTY(bool inverted READ inverted WRITE setInverted NOTIFY
               invertedChanged) /*!< Node::inverted */

  Q_PROPERTY(
    QPoint pos READ pos WRITE setPos NOTIFY posChanged) /*!< \see Node::pos */
  Q_PROPERTY(QPoint inPos READ inPos WRITE setInPos NOTIFY
               inPosChanged) /*!< \see Node::inPos */
  Q_PROPERTY(QPoint outPos READ outPos WRITE setOutPos NOTIFY
               outPosChanged) /*!< \see Node::outPos */

  Q_PROPERTY(
    double min READ min WRITE setMin NOTIFY minChanged) /*!< \see Node::min */
  Q_PROPERTY(
    double max READ max WRITE setMax NOTIFY maxChanged) /*!< \see Node::max */
  Q_PROPERTY(double first READ first WRITE setFirst NOTIFY
               firstChanged) /*!< \see Node::first */
  Q_PROPERTY(double second READ second WRITE setSecond NOTIFY
               secondChanged) /*!< \see Node::second */

public:
  /*!
   * \brief Default constructor.
   * \param parent Parent of the Node to create.
   */
  explicit Node(QObject* parent = nullptr);

  /*!
   * \brief The Type enum is used to specify the type of nodes.
   *
   * \details Can also be used to specify the direction in a connection betwen
   * two nodes.
   */
  enum Type
  {
    Input = 0, /*!< Input node. Receives messages from the network. Send data to
                  gates and output ndoes. */
    Output = 1,  /*!< Output node. Receives messages from gates and input nodes.
                    Sends messages over the network.*/
    OrGate = 10, /*!< Or gate. Combines multiple input nodes and gates. Acts
                    like a logical OR gate. */
    AndGate = 11 /*!< And gate. Combines multiple input nodes and gates. Acts
                    like a logical AND gate. */
  };

  Q_ENUM(Type)

  /*!
   * \brief Get the current position of the node.
   * \return The current position of the node in the room.
   */
  QPoint pos() const;

  /*!
   * \brief Set the position of a node.
   * \param point QPoint to set the position to.
   */
  void setPos(const QPoint& point);

  /*!
   * \brief Get the position of the slot for incoming connections.
   * \return A QPoint, positionned at the vertical center of the node, on the
   * far left.
   */
  QPoint inPos() const;

  /*!
   * \brief Get the position of the slot for incoming connections.
   * \param point Qpoint to set the position to.
   */
  void setInPos(const QPoint& point);

  /*!
   * \brief Get the position of the slot for outgoing connections.
   * \return A QPoint, positioned at the vertical center of the node on the far
   * right.
   */
  QPoint outPos() const;

  /*!
   * \brief Set the position of the slot for outgoing connections.
   * \param point QPoint to set the position to.
   */
  void setOutPos(const QPoint& point);

  /*!
   * \brief Set the type of the node
   * \param type Node::Type value specifying what kind of nodes, the backend
   * represents visually.
   */
  void setType(const Type& type);

  /*!
   * \brief Get the type of visual node the backend represents.
   * \return A Node::Type value.
   */
  Type type() const;

  /*!
   * \brief Set the value of the node.
   * \param value The value to give the node.
   */
  void setValue(const double& value);

  /*!
   * \brief Get the current value of the node.
   * \return The value of the node.
   */
  double value() const;

  /*!
   * \brief Get the current list of arguments to send with the OSC message the
   * node might send.
   *
   * \return A list of float values.
   */
  QList<double> argumentsValues() const;

  /*!
   * \brief Get the current list of values to be sent as OSC arguments as a
   * comma separated list of floats.
   *
   * \return A QString of comma separated values.
   */
  QString arguments() const;

  /*!
   * \brief Set the list of OSC arguments from a comma separated string of
   * values.
   *
   * \param args The string to set as arguments.
   */
  void setArguments(const QString& args);

  /*!
   * \brief Change the name of the node.
   * \param name The string to set the node's name to.
   */
  void setName(const QString& name);

  /*!
   * \brief Get te name of the node.
   * \return The node's current name.
   */
  QString name() const;

  /*!
   * \brief Set the OSC address to send/read messages to/from.
   * \param address The OSC address to use.
   */
  void setAddress(const QString& address);

  /*!
   * \brief Get the OSC address of the node.
   * \return The OSC address to send/read to/from as a string.
   */
  QString address() const;

  /*!
   * \brief Change the boolean value of the node.
   * \param value The new value to set the node to.
   */
  void setOutput(const bool& value);

  /*!
   * \brief Get the output value of the node.
   * \return The current boolean value of the node.
   */
  bool output() const;

  /*!
   * \brief Toggle the chain to bind/unbind maximum and minimum value of the
   * slider. \param b Boolean specifying st which state to set the chain.
   */
  void setBound(const bool& b);

  /*!
   * \brief Get the current state of the chain.
   * \return The current state of the chain.
   * \see Node::setBound
   */
  bool bound() const;

  /*!
   * \brief Toggle the node between closed and opened.
   * \param b Boolean specifying at which state to put the node.
   */
  void setOpened(const bool& b);

  /*!
   * \brief Get the opened/closed state of the node.
   * \return  A boolean specifying the current state of the node.
   */
  bool opened() const;

  /*!
   * \brief Toggle the node between inverted and normal.
   * \param b Boolean specifying at which state to put the node.
   */
  void setInverted(const bool& b);

  /*!
   * \brief Get the inverted/normal state of the node.
   * \return A boolean specifying the current state of the node.
   */
  bool inverted() const;

  /*!
   * \brief Set the minimum value of the slider.
   * \param rangeStart The value to set the minimum to.
   */
  void setMin(const double& rangeStart);

  /*!
   * \brief Get the minimum value of the slider.
   * \return The current minimum value of the slider.
   */
  double min() const;

  /*!
   * \brief Set the maximum value of the slider
   * \param rangeEnd The value to set the minimum to.
   */
  void setMax(const double& rangeEnd);

  /*!
   * \brief Get the maximum value of the slider.
   * \return The current maximum value of the slider.
   */
  double max() const;

  /*!
   * \brief Set the value of the first handle of the slider.
   * \param firstHandle The value to set the first handle to.
   */
  void setFirst(const double& firstHandle);

  /*!
   * \brief Get the value of the first handle of the slider.
   * \return The current value of the first handle of the slider.
   */
  double first() const;

  /*!
   * \brief Set the value of the second handle of the slider.
   * \param secondHandle The value to set the second handle to.
   */
  void setSecond(const double& secondHandle);

  /*!
   * \brief Get the value of the second handle of the slider.
   * \return The current value of the second handle of the slider.
   */
  double second() const;

  /*!
   * \brief Set the IP address to send osc messages to.
   * \param ip A string (ex: "127.0.0.1:8080") to set the IP to.
   */
  void setIp(const QString& ip);

  /*!
   * \brief Get the current IP address messages are sent to.
   * \return A string with the ip an port separated by a colon.
   */
  QString ip() const;

  /*!
   * \brief Get the IP messages are to be sent to.
   * \return A QHostAddress instance with only the IP.
   * \see Node::ip
   */
  QHostAddress getIp() const;

  /*!
   * \brief Get the port messages are to be sent to.
   * \return A quint16 consisting of the port.
   * \see Node::ip
   */
  quint16 getPort() const;

  /*!
   * \brief Get the unique id representing this node, for pointer swizzling.
   * \return A 64 bit integer uniquely identifying the node in the current
   * session.
   */
  qulonglong id() const;

  /*!
   * \brief Read a json object and update the node's values accordingly.
   * \param json The json object to be read.
   * \param version File format version we're reading/
   */
  void read(const QJsonObject& json, qint64 version = 1);

  /*!
   * \brief Write the state of the node in a json object.
   * \return The json object in which the state have been written.
   */
  QJsonObject write() const;

  /*!
   * \brief readArguments Read a list of double values from a json array.
   * \param jsonArgs The json array to read from.
   * \return A QList of doubles containing all the values successfully read.
   */
  QList<double> readArguments(const QJsonArray& jsonArgs);

  /*!
   * \brief writeList Serialize a list of double into a json array.
   * \param list The list to serialize.
   * \return A QJsonArray containing all the values in the list.
   */
  QJsonArray writeList(const QList<double>& list) const;

  /*!
   * \brief Read a json object containing a x and a y field.
   * \param jsonPoint The json object to be read.
   * \return A QPoint formed of the two coordinates read from the json object.
   */
  QPoint readPoint(const QJsonObject& jsonPoint) const;

  /*!
   * \brief Serialize a QPoint into a json object.
   * \param jsonPoint The point to be serialized.
   * \return The json object in which the point have been written.
   */
  QJsonObject writePoint(const QPoint& jsonPoint) const;

private:
  QString m_name = "Name"; /*!< Name of the node. Keep as unique as possible. */
  QString m_address = "/home/default"; /*!< OSC address messages are sent to. */
  QString m_ip = "127.0.0.1:8888";     /*!< Combination of IP and port the OSC
                                          messages are sent to. */

  QPoint m_pos; /*!< The position of the node. */
  QPoint m_in;  /*!< The position of the slot for incoming connections. */
  QPoint m_out; /*!< The position of the slot for outgoing connections. */

  Type m_type = Node::Input; /*!< The type of the node. */

  QList<double>
    m_arguments; /*!< Arguments to send with the next OSC message. */
  QString m_argumentsString; /*!< Comma sepated string of the elements in
                                Node::m_arguments. */

  double m_value = 0.0;   /*!< Value of the node. */
  double m_min = 0.0;     /*!< Minimum value of the slider in an input node. */
  double m_max = 100.0;   /*!< Maximum value of the slider in an input node. */
  double m_first = 25.0;  /*!< Value of the first handle of the slider. */
  double m_second = 75.0; /*!< Value of the first handle of the slider. */

  bool m_output = true; /*!< Output value of the node. */
  bool m_bound = false; /*!< Boolean specifying wether the two handles of the
                           slider are bound. */
  bool m_opened =
    true; /*!< Boolean specifying wether the node is in function or not. */
  bool m_inverted = false; /*!< Boolean specifying if the output (\see
                              Node::m_output) of the node is inverted or not. */

  qulonglong m_id; /*!< 64 bit integer uniquely identifying the node in the
                      current session. */

signals:
  // basic
  void typeChanged();    /*!< Notify the node's type have changed. */
  void nameChanged();    /*!< Notify the node's name have changed. */
  void addressChanged(); /*!< NOtify the node's OSC address have changed. */
  void ipChanged();      /*!< Notify the node's IP address have changed. */
  void
  connectionsHaveChanged(); /*!< Notify the connections the node is part of have
                               changed (new connection/connection broken). */
  void posChanged();        /*!< Notify the node's position have changed. */
  void inPosChanged();      /*!< Notify the position of the slot for incoming
                               connections have changed. */
  void outPosChanged();     /*!< Notify the position of the slot for out going
                               connections have changed. */
  void outputChanged();     /*!< Notify the node's output value have changed. */
  void
  boundChanged(); /*!< Notify the node's bound/unbound state have changed. */
  void openedChanged();   /*!< Notify the node's on/off state have changed. */
  void invertedChanged(); /*!< Notify the node's inverted/normal state have
                             changed. */
  void messageReady(
    Node*);               /*!< Notify when a node is ready to send a message. */
  void valueChanged();    /*!< Notify the value of the ndoe have changed. */
  void nodeHaveChanged(); /*!< NOtify any kind of change in the node. */

  // input
  void
  minChanged(); /*!< Notify when the slider's minimum value have changed. */
  void
  maxChanged(); /*!< Notify when the slider's maximum value have changed. */
  void firstChanged();  /*!< Notify when the slider's first handle's value have
                           changed. */
  void secondChanged(); /*!< Notify when the slider's second hadle's value have
                           changed. */

  // output
  void conditionOverriden(); /*!< Notify when the override button have been
                                pressed. */
  void argumentsUpdated();   /*!< Notify when the list of OSC arguments have
                                changed. */
};
