
# Table of Contents

1.  [What is it?](#orgc945d47)
    1.  [Programming interface](#org5d1f43f)
    2.  [Visualizer](#orged89c19)
    3.  [Controller](#org4f57e98)
    4.  [All that and much more](#orgea02765)
2.  [How is it made?](#org3151beb)
3.  [Credits](#org506dfcb)
4.  [Usage and contributions](#org3765a49)

[WIP]

[![pipeline status](https://gitlab.com/hantz/Neo/badges/master/pipeline.svg)](https://gitlab.com/hantz/Neo/commits/master) [![coverage report](https://gitlab.com/hantz/Neo/badges/master/coverage.svg)](https://gitlab.com/hantz/Neo/commits/master)

<a id="orgc945d47"></a>

# What is it?

![img](./res/logo.png)

neo (all lowercase) is a **visual programming language** and a **real time visualizer
and controller** for objects connected over a network.


<a id="org5d1f43f"></a>

## Programming interface

As a programming interface, neo allows the creation of complexed scenarios based
on behaviour of the objects (sensors) it is monitoring.

![img](./res/buttonOrLightSensorDoorAllRed.png "Figure 1")

In the above figure, the scenario involves a light sensor, a button and a door
connected through an OR gate. Whenever the button OR the light sensor change
in a certain way, the door is instructed to pose a predefined action.


<a id="orged89c19"></a>

## Visualizer

As a visualizer, neo makes it easy to track the status of sensors in real time.

![img](./res/lightSensorOutputGreen.png "Figure 2")


<a id="org4f57e98"></a>

## Controller

As a controller, neo helps users intervening when things are not working the way
by allowing them to expressly instruct an object to execute a predefined action
without physical interaction.

![img](./res/outputPopup.png "Figure 3")


<a id="orgea02765"></a>

## All that and much more

These 3 functions are not everything. They can be used at the same or one at a
time. The interface is designed to be flexible and easy to modify.


<a id="org3151beb"></a>

# How is it made?

The program is made using QT. The UI is a combination of QML and JavsScript
while the application logic is written in C++.

Development is cuurently sponsored by [Mission Morphéus](https://www.missionsmorpheus.com/).


<a id="org506dfcb"></a>

# Credits

-   Logo and icon: [Nick Morris](https://nickmorris.netlify.com/)


<a id="org3765a49"></a>

# Usage and contributions

[neo on GitLab](https://gitlab.com/hantz/Neo)

**Coming soon.**

