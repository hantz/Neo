#include <QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "connection.h"
#include "log.h"
#include "room.h"

int
main(int argc, char* argv[])
{

// windows fonts
#ifdef Q_OS_WIN
  qputenv("QML_DISABLE_DISTANCEFIELD", "1");
#endif

  QCoreApplication::setOrganizationName("hantz");
  QCoreApplication::setOrganizationDomain("hantz.gitlab.io");
  QCoreApplication::setApplicationName("neo");
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QGuiApplication app(argc, argv);

  qmlRegisterType<Node>("Neo.Node", 1, 0, "Node");
  qmlRegisterType<Room>("Neo.Room", 1, 0, "Room");
  qmlRegisterType<Connection>("Neo.Connection", 1, 0, "Connection");
  qmlRegisterType<Log>("Neo.Log", 1, 0, "Log");

  QQmlApplicationEngine engine;
  engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

  if (engine.rootObjects().isEmpty())
    return -1;

  return app.exec();
}
