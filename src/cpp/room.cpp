#include "room.h"
#include <QDebug>

/*!
 * \details Slot/Signal connections are initialized in this class.
 */
Room::Room(QObject* parent)
{
  Q_UNUSED(parent)

  m_host = QHostAddress(
    m_settings.value("configs/network/host", "127.0.0.1").toString());
  m_port = static_cast<quint16>(
    m_settings.value("configs/network/port", 8888).toInt());

  connect(this, &Room::roomHaveChanged, [=]() { this->m_changeSaved = false; });

  connect(this, &Room::hostChanged, [=]() {
    m_settings.setValue("configs/network/host", m_host.toString());
  });

  connect(this, &Room::portChanged, [=]() {
    m_settings.setValue("configs/network/port", m_port);
  });
}

/*!
 */
void
Room::clearNodes(QQmlListProperty<Node>* l)
{
  static_cast<Room*>(l->object)->m_nodes.clear();
  emit static_cast<Room*>(l->object)->nodesUpdated();
}

/*!
 */
void
Room::clearConnections(QQmlListProperty<Connection>* l)
{
  static_cast<Room*>(l->object)->m_connections.clear();
  emit static_cast<Room*>(l->object)->connectionsUpdated();
}

void
Room::clearLogs(QQmlListProperty<Log>* l)
{
  static_cast<Room*>(l->object)->m_logs.clear();
  emit static_cast<Room*>(l->object)->logsUpdated();
}

/*!
 */
void
Room::addNode(QQmlListProperty<Node>* l, Node* n)
{
  auto r = static_cast<Room*>(l->object);
  r->m_nodes.append(n);
  connect(n, &Node::messageReady, r, &Room::sendMessage);
  connect(n, &Node::nodeHaveChanged, r, &Room::roomHaveChanged);
  emit r->nodesUpdated();
}

/*!
 */
void
Room::addConnection(QQmlListProperty<Connection>* l, Connection* c)
{
  static_cast<Room*>(l->object)->m_connections.append(c);
  emit static_cast<Room*>(l->object)->connectionsUpdated();
}

void
Room::addLog(QQmlListProperty<Log>* list, Log* log)
{
  static_cast<Room*>(list->object)->m_logs.append(log);
  emit static_cast<Room*>(list->object)->logsUpdated();
}

/*!
 */
int
Room::countNodes(QQmlListProperty<Node>* l)
{
  return static_cast<Room*>(l->object)->m_nodes.length();
}

/*!
 */
int
Room::countConnections(QQmlListProperty<Connection>* l)
{
  return static_cast<Room*>(l->object)->m_connections.length();
}

int
Room::countLogs(QQmlListProperty<Log>* l)
{
  return static_cast<Room*>(l->object)->m_logs.length();
}

/*!
 */
Node*
Room::getNode(QQmlListProperty<Node>* l, int i)
{
  return static_cast<Room*>(l->object)->m_nodes.at(i);
}

/*!
 */
Connection*
Room::getConnection(QQmlListProperty<Connection>* l, int i)
{
  return static_cast<Room*>(l->object)->m_connections.at(i);
}

Log*
Room::getLog(QQmlListProperty<Log>* l, int i)
{
  return static_cast<Room*>(l->object)->m_logs.at(i);
}

/*!
 */
QQmlListProperty<Node>
Room::nodes()
{
  return QQmlListProperty<Node>(this,
                                &m_nodes,
                                &Room::addNode,
                                &Room::countNodes,
                                &Room::getNode,
                                &Room::clearNodes);
}

/*!
 */
QQmlListProperty<Connection>
Room::connections()
{
  return QQmlListProperty<Connection>(this,
                                      &m_connections,
                                      &Room::addConnection,
                                      &Room::countConnections,
                                      &Room::getConnection,
                                      &Room::clearConnections);
}

QQmlListProperty<Log>
Room::logs()
{
  return QQmlListProperty<Log>(this,
                               &m_logs,
                               &Room::addLog,
                               &Room::countLogs,
                               &Room::getLog,
                               &Room::clearLogs);
}

/*!
 */
QUrl
Room::file() const
{
  return QUrl(m_file.fileName());
}

void
Room::setFile(const QUrl& url)
{
  m_file.setFileName(url.toLocalFile());
  emit fileChanged();
  log(new Log("File saving", "File name changed", Log::Info));
}

QString
Room::host() const
{
  return m_host.toString();
}

void
Room::setHost(const QString& host)
{
  m_host = QHostAddress(host);
  emit hostChanged();
}

quint16
Room::port() const
{
  return m_port;
}

void
Room::setPort(const quint16& port)
{
  m_port = port;
  emit portChanged();
}

/*!
 * \details Also breaks any connection involving that node.
 */
bool
Room::deleteNode(Node* node)
{
  removeConnections(node);
  auto b = m_nodes.removeOne(node);
  nodesUpdated();
  emit connectionsUpdated();
  return b;
}

/*!
 */
void
Room::removeConnections(Node* n)
{
  QMutableListIterator<Connection*> it(m_connections);
  while (it.hasNext()) {
    auto c = it.next();
    if (c->receiver() == n || c->sender() == n) {
      it.remove();
    }
  }
  emit connectionsUpdated();
}

/*!
 * \details The parameters behave like Room::connected
 */
void
Room::createConnection(Node* a, Node* b, int direction)
{
  // when connecting to output watch out for network loop
  if ((b->type() == Node::Output || a->type() == Node::Output) &&
      networkLoop(a, b, direction)) {
    log(new Log("Node connection",
                "The connection of these node would create an infinite loop.",
                Log::Warning));
    return;
  }

  Connection* c = new Connection();

  QVariant av = QVariant::fromValue(a);
  QVariant bv = QVariant::fromValue(b);

  switch (direction) {
    case Node::Input:
      c->setSender(a);
      c->setReceiver(b);
      break;
    case Node::Output:
      c->setSender(b);
      c->setReceiver(a);
      break;
  }

  m_connections.append(c);
}

/*!
 *  \details The \p direction parameter specifies how the other parameters
 * should be interpreted.
 *
 * Possible values:
 * - Node::Input
 * 	 Indicates that the first parameter (\p a) is the sender in a potential
 * connection.
 * - Node::Output
 * 	 Indicates that the first parameter (\p a) is the receiver in a
 * potential connection.
 *
 */
bool
Room::connected(Node* a, Node* b, int direction)
{
  for (auto c : m_connections) {
    switch (direction) {
      case Node::Input:
        if (c->sender() == a && c->receiver() == b) {
          return true;
        }
        break;
      case Node::Output:
        if (c->sender() == b && c->receiver() == a) {
          return true;
        }
        break;
    }
  }
  return false;
}

/*!
 * \details Mainly to check for possible loops before establishing a connection.
 * The parameters behave like Room::connected.
 */
bool
Room::canConnect(Node* a, Node* b, int direction)
{
  // can't connect to self
  if (a == b) {
    return false;
  }

  if (a->type() == b->type()) {
    // only gates can connect to same type
    if (a->type() != Node::AndGate && a->type() != Node::OrGate) {
      return false;
    }
  }

  // gates
  if ((a->type() == Node::AndGate || a->type() == Node::OrGate) &&
      (b->type() == Node::AndGate || b->type() == Node::OrGate)) {
    // watch out for loops when connecting gates to gates
    if (looping(a, b, direction)) {
      return false;
    }
  }

  return true;
}

/*!
 * \details The parameters are inteerpreted like in Room::canConnect.
 */
bool
Room::looping(Node* a, Node* b, int direction)
{
  if (connected(a, b, direction == Node::Input ? Node::Output : Node::Input)) {
    return true;
  }

  if (direction == Node::Input) {
    for (auto c : m_connections) {
      if (c->sender() == b && looping(a, c->receiver(), direction)) {
        return true;
      }
    }
  } else {
    for (auto c : m_connections) {
      if (c->receiver() == b && looping(a, c->sender(), direction)) {
        return true;
      }
    }
  }

  return false;
}

/*!
 * \details A network loop is created when an output node (type Node::Output)
 * and an input node (type Node::Input) are connected and have the same OSC
 * address.
 */
bool
Room::networkLoop(Node* a, Node* b, int direction)
{
  if (direction == Node::Input) {
    if (a->type() == Node::Input) {
      return a->address() == b->address();
    }

    for (auto c : m_connections) {
      if (c->sender()->type() == Node::Input &&
          c->sender()->address() == b->address() && chained(c->sender(), a)) {
        return true;
      }
    }
  } else {
    if (b->type() == Node::Input) {
      return a->address() == b->address();
    }

    for (auto c : m_connections) {
      if (c->sender()->type() == Node::Input &&
          c->sender()->address() == a->address() && chained(c->sender(), b)) {
        return true;
      }
    }
  }

  return false;
}

/*!
 * \details If \p b can be reached by recursively exploring the connection
 * list starting from \p a, the nodes are considered chained.
 */
bool
Room::chained(Node* a, Node* b)
{
  if (connected(a, b)) {
    return true;
  }

  for (auto c : m_connections) {
    if (c->sender() == a && chained(c->receiver(), b)) {
      return true;
    }
  }

  return false;
}

/*!
 */
bool
Room::hasInConnection(Node* n) const
{
  if (n->type() == Node::Input) {
    return false;
  }

  for (auto c : m_connections) {
    if (c->receiver() == n) {
      return true;
    }
  }

  return false;
}

/*!
 */
bool
Room::hasOutConnection(Node* n) const
{

  if (n->type() == Node::Output) {
    return false;
  }

  for (auto c : m_connections) {
    if (c->sender() == n) {
      return true;
    }
  }

  return false;
}

/*!
 * \details The parameters behave like Room::connected.
 */
void
Room::removeConnections(Node* a, Node* b, int direction)
{
  QMutableListIterator<Connection*> it(m_connections);

  while (it.hasNext()) {
    auto c = it.next();
    switch (direction) {
      case Node::Input:
        if (c->sender() == a && c->receiver() == b) {
          it.remove();
        }
        break;
      case Node::Output:
        if (c->sender() == b && c->receiver() == a) {
          it.remove();
        }
        break;
    }
  }
}

/*!
 */
bool
Room::getValue(Node* n) const
{

  if (!n->opened()) {
    return false;
  }

  bool res = false;
  bool done = false;
  switch (n->type()) {
    case Node::AndGate:
    case Node::Output:
      if (hasInConnection(n)) {
        for (auto c : m_connections) {
          if (!(c->sender()->output()) && c->receiver() == n) {
            res = false;
            done = true;
            break;
          }
        }
        if (!done) {
          res = true;
        }
      }
      break;
    case Node::OrGate:
      if (hasInConnection(n)) {
        for (auto c : m_connections) {
          if ((c->sender()->output()) && c->receiver() == n) {
            res = true;
            break;
          }
        }
      }
      break;
    case Node::Input:
      res = n->value() <= n->second() && n->value() >= n->first();
  }

  return res ^ n->inverted();
}

/*!
 */
void
Room::evaluate(Node* n)
{
  n->setOutput(getValue(n));

  for (auto c : m_connections) {
    if (c->sender() == n) {
      c->receiver()->connectionsHaveChanged();
    }
  }
}

/*!
 */
void
Room::bindSocket()
{
  m_sock.close();
  if (m_sock.bind(m_host, m_port)) {
    connect(&m_sock, &QUdpSocket::readyRead, this, &Room::readPendingDatagrams);
    log(new Log("Socket initialization",
                "Listening on: " + m_sock.localAddress().toString() +
                  ", port: " + QString::number(m_sock.localPort()),
                Log::Info));
  } else {
    log(new Log("Socket initialization", m_sock.errorString(), Log::Error));
  }
}

/*!
 */
void
Room::readPendingDatagrams()
{
  while (m_sock.hasPendingDatagrams()) {
    QNetworkDatagram dg(m_sock.receiveDatagram());

    try {
      OscReader reader(new QByteArray(dg.data()));

      if (reader.getContentType() == OscContent::Bundle) {
        processBundle(reader.getBundle());
      } else {
        processMessage(reader.getMessage());
      }
    } catch (QException& qe) {
      log(new Log("OSC reception", qe.what(), Log::Error));
    }
  }
}

/*!
 */
void
Room::sendMessage(Node* n)
{
  OscMessageComposer mc(n->address());
  for (auto a : n->argumentsValues()) {
    mc.pushInt32(static_cast<qint32>(a));
  }
  m_sock.writeDatagram(*(mc.getBytes()), n->getIp(), n->getPort());
}

/*!
 */
void
Room::processBundle(OscBundle* bundle)
{
  for (auto i = 0; i < bundle->getNumEntries(); ++i) {
    if (bundle->getType(i) == OscContent::Bundle) {
      processBundle(bundle->getBundle(i));
    } else {
      processMessage(bundle->getMessage(i));
    }
  }
}

/*!
 */
void
Room::processMessage(OscMessage* message)
{
  log(new Log("New OSC message",
              "Received OSC message for address: " + message->getAddress() +
                " with " + QString::number(message->getNumValues()) +
                " arguments."));
  for (auto n : m_nodes) {
    if (n->type() == Node::Input && n->address() == message->getAddress() &&
        message->getNumValues() > 0) {
      n->setValue(message->getValue(0)->toDouble());
    }
  }
}

/*!
 * \details Serializes every node and connection created in this room and write
 * them to a file.
 *
 * \pre This function assumes a file have been set already by calling
 * Room::setFile.
 *
 */
void
Room::save()
{
  if (!m_file.open(QIODevice::WriteOnly)) {
    log(new Log(
      "File saving", "Could not open file: " + m_file.fileName(), Log::Error));
    return;
  }

  QJsonDocument dataDoc(write());
  m_file.write(dataDoc.toBinaryData());

  m_changeSaved = true;
  m_file.close();
}

void
Room::save(const QUrl& url)
{
  m_file.setFileName(url.toLocalFile().endsWith(".neo")
                       ? url.toLocalFile()
                       : url.toLocalFile() + ".neo");
  save();
}

/*!
 * \details The content of the file is loaded into json objects.
 *
 * This method does not create instances of Node or Connection for the loaded
 * data.
 *
 * \post The content of the lists Room::m_jsonNodes and Room::m_jsonConnections
 * should be consumed after calling this function.
 */
void
Room::load(const QUrl& url)
{
  QFile file(url.toLocalFile());
  if (!file.open(QIODevice::ReadOnly)) {
    log(new Log(
      "File loading", "Could not open file: " + url.toLocalFile(), Log::Error));
    return;
  }

  QByteArray dataArray = file.readAll();
  file.close();

  QJsonDocument dataDoc = QJsonDocument::fromBinaryData(dataArray);
  read(dataDoc.object());

  emit roomLoaded();
}

/*!
 * \details This method can be used to determine if a file name have been set
 * before calling Room::save.
 *
 * \pre The room should be cleared of all its nodes and connections before
 * loading new data in it.
 */
bool
Room::savedBefore() const
{
  return m_file.exists();
}

/*!
 */
bool
Room::changesSaved() const
{
  return m_changeSaved;
}

/*!
 */
void
Room::read(const QJsonObject& json)
{

  // read header
  if (json.contains("header") && json["header"].isObject()) {
    QJsonObject header = json["header"].toObject();
    if (header.contains("version") && header["version"].isString()) {
      QString vstring = header["version"].toString();
      bool ok;
      qint64 version = vstring.toInt(&ok);
      if (ok) {
        m_jsonVersion = version;
      }
    }
  }

  // Don't attempt to load files we can't read
  if (m_jsonVersion > MAX_SUPPORTED_VERSION) {
    log(new Log("File reading",
                "Version " + QString::number(m_jsonVersion) +
                  " is not supported. Max supported version is " +
                  QString::number(MAX_SUPPORTED_VERSION),
                Log::Warning));
    return;
  }

  // read nodes
  if (json.contains("nodes") && json["nodes"].isArray()) {
    m_jsonNodes = json["nodes"].toArray();
    m_nodes.clear();
  }

  // read connections
  if (json.contains("connections") && json["connections"].isArray()) {
    m_jsonConnections = json["connections"].toArray();
    m_connections.clear();
    m_nodeHash.clear();
  }
}

/*!
 */
QJsonObject
Room::write() const
{
  QJsonObject json;

  // write header
  QJsonObject header{ { "version", QString::number(MAX_SUPPORTED_VERSION) } };
  json["header"] = header;

  // write nodes
  QJsonArray nodeArray;
  for (const auto& n : m_nodes) {
    nodeArray.append(n->write());
  }
  json["nodes"] = nodeArray;

  // write connections
  QJsonArray connectionArray;
  for (const auto& c : m_connections) {
    connectionArray.append(c->write());
  }
  json["connections"] = connectionArray;

  return json;
}

/*!
 * \pre Room::load have to be called before using this method.
 * \post When this returns false, Room::loadConnections should be called
 * immediately after.
 */
bool
Room::hasMoreLoaded() const
{
  return !(m_jsonNodes.isEmpty());
}

/*!
 */
void
Room::loadNextNode(Node* n)
{
  if (!(m_jsonNodes.isEmpty())) {
    n->read(m_jsonNodes.takeAt(0).toObject(), m_jsonVersion);
    m_nodeHash.insert(n->id(), n);
    m_nodes.append(n);
    connect(n, &Node::messageReady, this, &Room::sendMessage);
    connect(n, &Node::nodeHaveChanged, this, &Room::roomHaveChanged);
  }
}

/*!
 * \pre Room::load should be called before using this method. All
 * nodes loaded as json objects should have also been loaded into Node objects
 * before calling this.
 */
void
Room::loadConnections()
{
  m_connections.reserve(m_jsonConnections.size());
  for (const auto json : m_jsonConnections) {
    Connection* c = new Connection();
    c->read(json.toObject(), m_nodeHash, m_jsonVersion);
    m_connections.append(c);
    c->sender()->connectionsHaveChanged();
    c->receiver()->connectionsHaveChanged();
  }
}

/*!
 * \pre Room::hasMoreLoaded have to be used before calling this method to
 * determine if there is more loaded data
 *
 */
int
Room::nextType()
{
  return Node::Type(m_jsonNodes.at(0).toObject()["type"].toInt());
}

void
Room::log(Log* l)
{
  m_logs.append(l);
  emit logsUpdated();
}
