#include "log.h"

// Log::Log(QObject* parent)
//{
//  Log("Unknown", "None.", Log::Log, parent);
//}

Log::Log(QString context, QString text, Log::Level l, QObject* parent)
  : QObject(parent)
{

  m_level = l;
  m_text = "[" + levelToString(m_level) + "] (" + context + ") : " + text;
}

QString
Log::text()
{
  return m_text;
}

Log::Level
Log::level()
{
  return m_level;
}

QString
Log::levelToString(Log::Level l)
{
  switch (l) {
    case Log::Debug:
      return "Debug";
    case Log::Info:
      return "Info";
    case Log::Warning:
      return "Warning";
    case Log::Error:
      return "Error";
  }

  return "Unknown";
}
