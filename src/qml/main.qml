import QtQuick 2.10
import QtQuick.Window 2.2
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import Neo.Log 1.0


/*! \brief Main window of the program.
        This is the outmost layer.
        Every other component is a (direct or indirect child) of this.
*/
ApplicationWindow {
    id: app

    width: 800
    height: 600

    visible: true

    title: qsTr("Neo")

    menuBar: NeoMenuBar {

        onClear: {
            if (room.backend.changesSaved()) {
                confirmDialog.confirmAndProceed(
                            room.clearAll, qsTr("Clear"), qsTr(
                                "Clearing the room will discard your changes.\nContinue without saving?"))
            } else {
                room.clearAll()
            }
        }

        onLoad: {
            if (!room.backend.changesSaved()) {
                confirmDialog.confirmAndProceed(function () {
                    fileDialog.state = "loading"
                    fileDialog.open()
                }, qsTr("Clear"), qsTr(
                    "Loading a new room will discard your changes.\nContinue without saving?"))
            } else {
                fileDialog.state = "loading"
                fileDialog.open()
            }
        }

        onSave: {
            if (room.backend.savedBefore()) {
                room.backend.save()
            } else {
                saveAs()
            }
        }

        onSaveAs: {
            fileDialog.state = "saving"
            fileDialog.open()
        }

        onQuit: {
            if (!room.backend.changesSaved()) {
                confirmDialog.confirmAndProceed(
                            Qt.quit, qsTr("Quit"), qsTr(
                                "Your changes aren't saved.\nQuit without saving?"))
            } else {
                Qt.quit()
            }
        }

        onSettings: {
            // TODO: launch pop up to edit ip address and port
            room.settings()
        }
    }

    onClosing: {
        close.accepted = false
        if (!room.backend.changesSaved()) {
            confirmDialog.confirmAndProceed(function () {
                Qt.quit()
            }, qsTr("Quit"),
            qsTr("Your changes aren't saved.\nQuit without saving?"))
        } else {
            Qt.quit()
        }
    }

    Column {
        anchors.fill: parent
        Rectangle {
            width: parent.width
            height: parent.height / 10 * 9
            NeoRoom {
                id: room
                anchors.fill: parent

                Component.onCompleted: {
                    backend.bindSocket()
                }
            }
        }

        Rectangle {
            id: statusBar

            ListView {
                anchors.fill: parent
                clip: true
                delegate: Rectangle {
                    width: statusBar.width
                    height: statusBar.height / 3
                    color: {
                        switch (modelData.level) {
                        case Log.Info:
                            return "#92bd6c"
                        case Log.Debug:
                            return "#245fa7"
                        case Log.Warning:
                            return "#976f3d"
                        case Log.Error:
                            return "#de4f4f"
                        }
                    }

                    border.width: 1
                    border.color: "#2e2f30"

                    Text {
                        anchors.fill: parent
                        text: modelData.text
                        font.bold: {
                            if (modelData.level === Log.Warning
                                    || modelData.level === Log.Error) {
                                true
                            } else {
                                false
                            }
                        }

                        font.underline: modelData.level === Log.Error

                        verticalAlignment: Text.AlignVCenter
                        leftPadding: 5
                    }
                }
                model: room.backend.logs
                maximumFlickVelocity: 500
                onCountChanged: positionViewAtEnd()
            }

            width: parent.width
            height: parent.height / 10
            color: "#2e2f30"
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a file"
        folder: shortcuts.home
        nameFilters: ["Neo files (*.neo)", "All files (*)"]
        property var states: ({
                                  loading: function (fileName) {
                                      room.clearAll()
                                      if (fileName !== undefined) {
                                          room.backend.load(fileName)
                                      }
                                  },
                                  saving: function (fileName) {
                                      if (fileName !== undefined) {
                                          room.backend.save(fileName)
                                      } else {
                                          room.backend.save()
                                      }
                                  }
                              })
        property string state: "loading"

        selectMultiple: false
        selectExisting: false
        selectFolder: false

        onAccepted: {
            states[state](fileDialog.fileUrl)
            close()
        }
    }

    MessageDialog {
        id: confirmDialog
        title: "generic title"
        text: "Is this a question?"
        property var proceed: undefined
        modality: Qt.WindowModal
        standardButtons: StandardButton.Ok | StandardButton.Cancel
        icon: StandardIcon.Question

        onAccepted: {
            close()
            proceed()
        }

        function confirmAndProceed(action, title, text) {
            proceed = action
            confirmDialog.title = title
            confirmDialog.text = text
            open()
        }
    }
}
