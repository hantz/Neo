import QtQuick 2.0
import QtQuick.Controls 2.3

Rectangle {
    id: inputField

    onFocusChanged: {
        textField.forceActiveFocus()
    }

    property string label: "FieldName"
    property string placeHolder: "Placeholder Text..."
    property string value
    property string inputMask
    property int inputMethodHints: Qt.ImhNone
    property var validator

    color: "transparent"
    Row {
        anchors.fill: parent
        clip: true
        spacing: 10
        Label {
            width: parent.width / 4
            height: parent.height
            leftPadding: 5
            text: inputField.label
            font.pixelSize: 12
            verticalAlignment: Text.AlignVCenter
        }

        TextField {
            id: textField

            width: parent.width / 4 * 3 - 10
            height: parent.height

            verticalAlignment: Text.AlignBottom
            clip: true

            font.pixelSize: 10
            placeholderText: inputField.placeHolder
            text: value
            inputMask: inputField.inputMask
            inputMethodHints: inputField.inputMethodHints
            validator: inputField.validator

            background: Rectangle {
                color: "transparent"
                border.width: {
                    textField.focus ? 2 : 1
                }

                border.color: {
                    textField.focus ? "blue" : "darkgrey"
                }
            }

            onEditingFinished: {
                inputField.value = text
            }
        }
    }
}
