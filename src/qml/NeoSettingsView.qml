import QtQuick 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import Neo.Room 1.0

Popup {
    id: popup

    signal reconnect

    property Room room

    Component.onCompleted: {
        ipField.value = room.host
        portField.value = room.port
    }

    Column {
        anchors.fill: parent
        TabBar {
            id: tabBar
            width: parent.width

            TabButton {
                id: networkTab

                text: qsTr("Network Settings")
            }
        }

        StackLayout {
            width: parent.width
            Layout.alignment: Qt.AlignBottom
            height: parent.height - tabBar.height - 50

            currentIndex: tabBar.currentIndex

            Rectangle {
                id: networkView
                color: "transparent"

                Column {
                    anchors.fill: parent
                    spacing: 5

                    Text {
                        id: title

                        width: parent.width

                        text: qsTr("Network")
                        font.bold: true
                        font.pixelSize: 14
                        horizontalAlignment: Text.AlignHCenter
                    }

                    NeoInputField {
                        id: ipField
                        width: parent.width
                        implicitHeight: 30

                        label: qsTr("Host IP")
                        placeHolder: qsTr("127.0.0.1")
                        validator: RegExpValidator {
                            regExp: /((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
                        }

                        onValueChanged: {
                            room.host = value
                        }
                    }

                    NeoInputField {
                        id: portField
                        width: parent.width
                        implicitHeight: 30

                        label: qsTr("Port")
                        inputMask: "0000"
                        inputMethodHints: Qt.ImhDigitsOnly

                        onValueChanged: {
                            room.port = value
                        }
                    }
                }
            }
        }

        RowLayout {
            width: parent.width
            spacing: 5

            Button {
                id: applyButton

                Layout.alignment: Qt.AlignRight

                text: qsTr("Apply and Reconnect")
                font.pixelSize: 12
                font.bold: true

                background: Rectangle {
                    color: "#d3d3d3"

                    radius: 2
                    border.width: {
                        applyButton.pressed ? 0 : 2
                    }

                    border.color: "darkgrey"
                }

                onClicked: {
                    reconnect()
                }
            }
        }
    }
}
